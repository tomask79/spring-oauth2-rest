package demo.resources.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingsController {

	@RequestMapping(value="/greeting")
	public String getGreetings() {
		return "Greetings from super secured rest!";
	}
}
