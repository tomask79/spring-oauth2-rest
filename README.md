# Securing REST Web Service with OAuth2 using Spring Cloud Security #

I have been thinking recently about what are the current posibilities of securing the REST web services. More or less standards remain the same:

* OAuth 2 (tokens)
* OAuth 1 (certificates)
* SAML
* CAS

In todays world of **Microservices** popularity, security becomes quite a chalange. One of widely used ways of securing them is **OAuth2** standard, which has excellent support in [Spring Framework](http://projects.spring.io/spring-framework/). Especially if you use [Spring Cloud Security](http://cloud.spring.io/spring-cloud-security/) module which is making creation of **Single Sign On** applications really easy. Let's see howto use it. First off all, if you're not familiar what OAuth2 security protocol is, please read following excellent article: [Introduction to OAuth2](https://www.digitalocean.com/community/tutorials/an-introduction-to-oauth-2)

My demo consists of three Spring-Boot applications making one application domain:

1. Separated authorization server 
2. Resource server (containing Rest web services)
3. UI server (Server which browser will be communicating with)

## Authorization server ##

Most important part of authorization server is the following method:


```
    @Override
	public void configure(ClientDetailsServiceConfigurer clients) 
			throws Exception {
		clients.inMemory()
        	.withClient("user")
        	.secret("userpassword")
        	.authorizedGrantTypes("authorization_code", "refresh_token",
        			"password").scopes("openid");
	}
```
First, we're registering here client_id 'user' and his secured password 'userpassword'. This user is allowed to by authorized with authorization grants **'authorization_code'**, 'refresh_token' and 'password'. See mentioned OAuth2 introduction link for more explanation.

## Resource server ##

Resource server is part of domain containing own REST web service which we want to have secured...Important part is setting of application class:


```
@SpringBootApplication
@EnableOAuth2Resource
public class ResourceServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResourceServerApplication.class, args);
    }
}
```
By adding **EnableOAuth2Resource** annotation we're adding OAuth2 filter containing logic for **security using OAuth2 security tokens**. Configuration of the mentioned logic can done with **spring.oauth2.resource.** properties in the Spring-Boot application.properties file.

Second important part is configuration in properties file:
```
spring.oauth2.resource.userInfoUri: http://localhost:8081/oauth-test/user
```

This tell server that **it can use the token to access entered endpoint in authorization server and use that for autentication.** Like authors say, it's like **me endpoint in the facebook API**. See implementation of /user endpoint in authorization server.

## UI Server ##

UI-Server is a server we're going to be communicating with. Again let's see the most important parts:

```
@SpringBootApplication
@EnableOAuth2Sso
@EnableZuulProxy
public class UiServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(UiServerApplication.class, args);
    }
}
```
By adding the **EnableOAuth2Sso** annotation we're enabling **Single Sign On** using OAuth2 tokens in server for our browser. And with Spring Cloud Security **EnableZuulProxy** annotation we're adding Zuul endpoint for proxying requests for resource server and authorization server..URL's can be configured in the yaml properties file, like this:


```
server:
  port: 9000	
zuul:
  routes:
    resource:
      path: /resources/**
      url: http://localhost:8080/greeting
    user:
      path: /user/**
      url: http://localhost:8081/oauth-test/user
spring:
  oauth2:
    sso:
      home:
        secure: false
        path: /,/**/*.html
    client:
      accessTokenUri: http://localhost:8081/oauth-test/oauth/token
      userAuthorizationUri: http://localhost:8081/oauth-test/oauth/authorize
      clientId: user
      clientSecret: userpassword
    resource:
      userInfoUri: http://localhost:8081/oauth-test/user
```
In this configuration we're telling that **we're going to be acting as user with clientId 'user' with client secret 'userPassword'**. Which is what third party REST web service provider will give you. We're also telling that we're going to be using separated authorization server at addresses set by parameters accessTokenUri, userAuthorizationUri.

### Howto test demo ###
Simply compile and run all servers. Now try to hit the following URL of Rest web service we've got secured:


```
http://localhost:8080/greeting
```

you will receive message that full autentication is required to access this URL.
Now hit URL, which requests full autentication and confirmation of accessing the resource:

```
http://localhost:9000/resources
```

After entering authentication credentials user/password you just confirm access to resource and that's it. You should see now result of my test Rest web service:

```
Greetings from super secured rest!
```

In addition, since now you've got loaded BEARER token to access any protected resource at resource server!

### Benefits of doing SSO applications like this ###

* Many third party vendors supports this standard, like openAM, which we use in Embedit.

* No problem to use this at mobile devices.