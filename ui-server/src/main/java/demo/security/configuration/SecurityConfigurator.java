package demo.security.configuration;

import org.springframework.cloud.security.oauth2.sso.OAuth2SsoConfigurerAdapter;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

@Configuration
public class SecurityConfigurator extends OAuth2SsoConfigurerAdapter {

	@Override
	public void match(RequestMatchers matchers) {
		matchers.anyRequest();
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/index.html", "/home.html", "/")
        	.permitAll().anyRequest().authenticated();
	}
 }
